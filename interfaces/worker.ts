import {saveWorkersStatuses} from "@/store";

export interface WorkerSimpleInterface {
    name: string;
    experience: number;
}

export interface WorkerInterface extends WorkerSimpleInterface {
    id: number;
}

export interface WorkerStateInterface {
    workers: WorkerInterface[],
    counter: number,
    saveWorkersStatus: saveWorkersStatuses,
}

export interface WorkerStoreInterface {
    state: WorkerStateInterface,
    getters: GettersInterface;
    mutations: MutationsInterface;
    actions: ActionsInterface;
}

export interface GettersInterface {
    [key: string]: { (state: WorkerStateInterface): WorkerInterface[] | undefined | WorkerInterface | saveWorkersStatuses };
}

export interface ActionsInterface {
    [key: string]: { (state: any, payload: any): void };
}

export interface MutationsInterface {
    [key: string]: { (state: WorkerStateInterface, payload: any): void };
}