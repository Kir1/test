import Vue from 'vue';
import Vuex from 'vuex';
import {
    WorkerInterface,
    WorkerStoreInterface,
} from '../../interfaces/worker';
import {findObjectAndAction} from "../../utils/array";

Vue.use(Vuex);

export enum workersEvents {
    WORKERS = 'WORKERS',
    BEST_WORKER = 'BEST_WORKER',
    ADD_WORKER = 'ADD_WORKER',
    REMOVE_WORKER = 'REMOVE_WORKER',
    UPDATE_WORKER = 'UPDATE_WORKER',
    POST_WORKERS = 'POST_WORKERS',
    CHANGE_SAVE_WORKERS_STATUS = 'CHANGE_SAVE_WORKERS_STATUS',
    SAVE_WORKERS_STATUS = 'SAVE_WORKERS_STATUS',
}

export enum saveWorkersStatuses {
    NORMAL = 'NORMAL',
    DONE = 'DONE',
    FAIL = 'FAIL',
}

export const storeOptions: WorkerStoreInterface = {
    state: {
        workers: [],
        counter: 0,
        saveWorkersStatus: saveWorkersStatuses.NORMAL,
    },
    getters: {
        [workersEvents.SAVE_WORKERS_STATUS](state) {
            return state.saveWorkersStatus;
        },
        [workersEvents.WORKERS](state) {
            return state.workers;
        },
        [workersEvents.BEST_WORKER](state) {
            if (state.workers.length > 0)
                return state.workers.reduce((acc: WorkerInterface, curr: WorkerInterface) => {
                    return acc.experience > curr.experience ? acc : curr
                });
            return undefined;
        },
    },
    mutations: {
        [workersEvents.ADD_WORKER](state, worker) {
            state.counter++;
            state.workers.push({...worker, id: state.counter});
        },
        [workersEvents.REMOVE_WORKER](state, worker) {
            findObjectAndAction(state.workers,
                (innerWorker) => worker.id === innerWorker.id,
                (index) => state.workers.splice(index, 1)
            );
        },
        [workersEvents.UPDATE_WORKER](state, worker) {
            findObjectAndAction(state.workers,
                (innerWorker) => worker.id === innerWorker.id,
                (index) => state.workers.splice(index, 1, worker)
            );
        },
        [workersEvents.CHANGE_SAVE_WORKERS_STATUS](state, status) {
            state.saveWorkersStatus = status;
        },
    },
    actions: {
        [workersEvents.POST_WORKERS]({commit, state}) {
            let status: saveWorkersStatuses;
            return new Promise((resolve, reject) => setTimeout(() => {
                    if (state.saveWorkersStatus === saveWorkersStatuses.NORMAL) {
                        resolve(saveWorkersStatuses.DONE);
                    }
                    reject(true);
                }, 300)
            ).then(statusInner => {
                status = statusInner as saveWorkersStatuses;
            }).catch(() => {
                status = saveWorkersStatuses.FAIL;
            }).finally(() => {
                commit(workersEvents.CHANGE_SAVE_WORKERS_STATUS, status);
            })
        },
    },
}

export default new Vuex.Store<WorkerStoreInterface>(storeOptions as any);

