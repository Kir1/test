export const findObjectAndAction = <Obj>(arr: Obj[], check: (obj: Obj) => boolean, action: (index: number) => void) => {
    const index = arr.findIndex(obj => check(obj));
    if (index > -1) action(index);
}