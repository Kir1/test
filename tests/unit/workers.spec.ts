import {shallowMount} from '@vue/test-utils';
import WorkerBlock from '@/components/worker/WorkerBlock.vue';
import WorkerForm from '@/components/worker/WorkerForm.vue';
import WorkerBest from '@/components/worker/WorkerBest.vue';
import WorkerList from '@/components/worker/WorkerList.vue';
import {createLocalVue} from '@vue/test-utils';
import Vuex from 'vuex';
import {storeOptions, workersEvents, saveWorkersStatuses} from '@/store';
import {cloneDeep} from 'lodash';
import {WorkerStateInterface} from "../../interfaces/worker";
import {Store} from 'vuex';

describe('worker', () => {
    const localVue = createLocalVue();
    localVue.use(Vuex);

    it('render components', () => {
        const wrapper = shallowMount(WorkerBlock);
        const workerBest = wrapper.findComponent(WorkerBest);
        const workerForm = wrapper.findComponent(WorkerForm);
        const workerList = wrapper.findComponent(WorkerList);
        expect(workerBest.exists()).toBe(true);
        expect(workerForm.exists()).toBe(true);
        expect(workerList.exists()).toBe(true);
    })

    it('add worker', async () => {
        const wrapper = shallowMount(WorkerForm);

        const textInput = wrapper.find('.worker-form__name');
        await textInput.setValue('Ivan');
        expect((wrapper.find('.worker-form__name').element as HTMLInputElement).value).toBe('Ivan');

        const textInput2 = wrapper.find('.worker-form__experience');
        await textInput2.setValue(10);
        const textInput2Value = Number((wrapper.find('.worker-form__experience').element as HTMLInputElement).value);
        expect(textInput2Value).toBe(10);

        const store: Store<WorkerStateInterface> = new Vuex.Store(cloneDeep(storeOptions));
        const finalWorkers = [...store.state.workers, {name: 'Ivan', experience: 10, id: 1}];
        store.commit(workersEvents.ADD_WORKER, {name: 'Ivan', experience: 10});
        expect(store.state.workers).toStrictEqual(finalWorkers);
    })

    it('save workers', async () => {
        const store: Store<WorkerStateInterface> = new Vuex.Store(cloneDeep(storeOptions));
        const wrapper = shallowMount(WorkerList, {store, localVue});
        wrapper.find('.workers__save').exists();
        await store.dispatch(workersEvents.POST_WORKERS);
        expect(store.state.saveWorkersStatus).toBe(saveWorkersStatuses.DONE);
        await store.dispatch(workersEvents.POST_WORKERS);
        expect(store.state.saveWorkersStatus).toBe(saveWorkersStatuses.FAIL);
    })
})
